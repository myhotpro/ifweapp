package com.ifwe.app.ui.activity;

import android.os.Bundle;


import com.ifwe.app.R;
import com.ifwe.app.data.DataManager;
import com.ifwe.app.data.MyPrefs;
import com.ifwe.app.ui.base.BaseActivity;
import com.ifwe.app.ui.fragment.SearchResultActivityFragment;
import com.ifwe.app.ui.presenter.SearchResultPresenter;

import javax.inject.Inject;

public class SearchResultActivity extends BaseActivity {

    public static final String KEY_SEARCH_TEXT = "key_search_text";

    @Inject
    DataManager dataManager;

    @Inject
    MyPrefs myPrefs;

    SearchResultPresenter searchResultPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myApp().getApplicationComponent().inject(this);
        SearchResultActivityFragment fragment = null;
        if (savedInstanceState == null) {
            fragment = SearchResultActivityFragment.newInstance(getSearchText());
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, fragment, SearchResultActivityFragment.TAG)
                    .commit();
        } else {
            fragment = (SearchResultActivityFragment) getSupportFragmentManager()
                    .findFragmentByTag(SearchResultActivityFragment.TAG);
        }

        searchResultPresenter = new SearchResultPresenter(fragment, dataManager, myPrefs);
    }

    private String getSearchText() {
        return getIntent().getStringExtra(KEY_SEARCH_TEXT);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_search_result;
    }

}
