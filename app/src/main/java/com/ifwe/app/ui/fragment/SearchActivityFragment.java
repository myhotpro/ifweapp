package com.ifwe.app.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


import com.ifwe.app.R;
import com.ifwe.app.ui.activity.SearchResultActivity;
import com.ifwe.app.ui.base.BaseFragment;

import butterknife.BindView;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * A placeholder fragment containing a simple view.
 */
public class SearchActivityFragment extends BaseFragment {

    private static final String TAG = SearchActivityFragment.class.getSimpleName();
    @BindView(R.id.et_search)
    EditText etSearch;

    @BindView(R.id.btn_search)
    Button btnSearch;

    public SearchActivityFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_search;
    }

    @Override
    protected void initView() {
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    btnSearch.setEnabled(true);
                } else {
                    btnSearch.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startSearch(etSearch.getText().toString());
            }
        });
    }

    private void startSearch(String text) {

        if (!isAdded()) {
            return;
        }
        final Intent intent = new Intent(getActivity(), SearchResultActivity.class);
        intent.putExtra(SearchResultActivity.KEY_SEARCH_TEXT, text);
        startActivity(intent);
    }

}
