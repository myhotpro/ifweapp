package com.ifwe.app.ui.contract;


import com.ifwe.app.data.model.SearchResult;
import com.ifwe.app.data.model.Status;
import com.ifwe.app.ui.base.MvpPresenter;
import com.ifwe.app.ui.base.MvpView;

/**
 * Created by yutao .
 */

public interface SearchResultContract {
    public interface Presenter extends MvpPresenter {
        void loadNext(String keyword, int start, int limit);
    }

    public interface View extends MvpView<Presenter> {
        void showNext(SearchResult searchResult);
        void showEmptyView();
        void showErrorView(String errorMsg);
        void showLoadingView();
        void showContentView();
    }
}
