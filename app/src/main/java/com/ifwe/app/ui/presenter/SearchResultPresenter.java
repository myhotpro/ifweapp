package com.ifwe.app.ui.presenter;

import android.text.TextUtils;

import com.ifwe.app.data.DataManager;
import com.ifwe.app.data.MyPrefs;
import com.ifwe.app.data.model.SearchResult;
import com.ifwe.app.data.model.Token;
import com.ifwe.app.ui.contract.SearchResultContract;

import org.w3c.dom.Text;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by yutao .
 */

public class SearchResultPresenter implements SearchResultContract.Presenter {

    private final SearchResultContract.View view;
    private final DataManager dataManager;
    private MyPrefs myPrefs;
    private final CompositeSubscription compositeSubscription;

    public SearchResultPresenter(
            SearchResultContract.View view,
            DataManager dataManager,
            MyPrefs myPrefs) {

        this.view = view;
        this.dataManager = dataManager;
        this.myPrefs = myPrefs;

        compositeSubscription = new CompositeSubscription();
        view.setPresenter(this);
    }

    @Override
    public void onResume() {
    }

    @Override
    public void onPause() {
    }

    @Override
    public void onDestroy() {
        compositeSubscription.clear();
    }

    @Override
    public void loadNext(final String keyword, int start, final int count) {

        Observable<SearchResult> obs = null;
        if (TextUtils.isEmpty(myPrefs.getAccessToken())) {
            obs = dataManager.getToken()
                    .flatMap(new Func1<Token, Observable<SearchResult>>() {
                        @Override
                        public Observable<SearchResult> call(Token token) {
                            myPrefs.setAccessToken(token.accessToken);
                            myPrefs.setTokenType(token.tokenType);
                            return dataManager.search("Bearer " + token.accessToken, keyword, count);
                        }
                    })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());

        } else {
            obs = dataManager.search("Bearer " + myPrefs.getAccessToken(), keyword, count)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());

        }
        Subscription subscription = obs.subscribe(new Subscriber<SearchResult>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                view.showErrorView(e.getMessage());
            }

            @Override
            public void onNext(SearchResult searchResult) {
                if (searchResult.getStatuses().size() == 0) {
                    view.showEmptyView();
                } else {
                    view.showContentView();
                    view.showNext(searchResult);
                }
            }
        });
        compositeSubscription.add(subscription);
    }
}
