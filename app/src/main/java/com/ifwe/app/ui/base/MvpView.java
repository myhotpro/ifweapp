package com.ifwe.app.ui.base;

/**
 * Created by yutao .
 */

public interface MvpView<T> {
    void setPresenter(T presenter);
}
