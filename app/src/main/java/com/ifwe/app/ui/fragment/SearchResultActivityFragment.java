package com.ifwe.app.ui.fragment;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.ifwe.app.R;
import com.ifwe.app.data.model.SearchResult;
import com.ifwe.app.data.model.Status;
import com.ifwe.app.ui.activity.SearchResultActivity;
import com.ifwe.app.ui.adapter.SearchResultAdapter;
import com.ifwe.app.ui.base.BaseFragment;
import com.ifwe.app.ui.contract.SearchResultContract;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * A placeholder fragment containing a simple view.
 */
public class SearchResultActivityFragment extends BaseFragment implements SearchResultContract.View {

    public static final String TAG = SearchResultActivityFragment.class.getSimpleName();
    public static final int PAGE_MAX_LIMIT = 20;

    @BindView(R.id.error_container)
    View errorView;

    @BindView(R.id.tv_error)
    TextView tvError;

    @BindView(R.id.btn_retry)
    Button btnRetry;

    @BindView(R.id.empty_container)
    View emptyView;

    @BindView(R.id.loading_container)
    View loadingView;

    @BindView(R.id.content_container)
    View contentView;

    @BindView(R.id.tv_total)
    TextView tvTotal;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    // page
    private int next;
    private int total;

    String searchText;
    private SearchResultContract.Presenter presenter;
    private SearchResultAdapter adapter;

    public SearchResultActivityFragment() {
    }

    private boolean isLoading;
    private RecyclerView.OnScrollListener mRecyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {


        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (dy > 0) {
                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
                String l = String.format("visibleItemCount: %d, totalItemCount: %d, firstVisibleItemPosition: %d",
                        visibleItemCount,
                        totalItemCount,
                        firstVisibleItemPosition);
                Log.d(TAG, "onScrolled " + l);
                if (!isLoading
                        && hasNextPage()
                        && (visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                    isLoading = true;
                    int limit = getLimit();
                    presenter.loadNext(searchText, next, limit);
                }
            }
        }
    };

    private int getLimit() {
        return PAGE_MAX_LIMIT;
    }

    private boolean hasNextPage() {
        return next < total;
    }

    public static SearchResultActivityFragment newInstance(String searchText) {
        SearchResultActivityFragment fragment = new SearchResultActivityFragment();
        Bundle bundle = new Bundle();
        bundle.putString(SearchResultActivity.KEY_SEARCH_TEXT, searchText);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            searchText = getArguments().getString(SearchResultActivity.KEY_SEARCH_TEXT);
            Log.d(TAG, "onCreate searchText: " + searchText);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("list", (ArrayList<Status>)adapter.getStatusList());
        outState.putInt("total", total);
        outState.putInt("next", next);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_search_result;
    }

    @Override
    protected void initView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        adapter = new SearchResultAdapter(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(mRecyclerViewOnScrollListener);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.loadNext(searchText, next, getLimit());
            }
        });
        if (savedInstanceState == null) {
            this.presenter.loadNext(searchText, next, getLimit());
            isLoading = true;
            showLoadingView();
        } else {
            next = savedInstanceState.getInt("next");
            total = savedInstanceState.getInt("total");
            List<Status> list = savedInstanceState.getParcelableArrayList("list");
            adapter.appendData(list);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void setPresenter(SearchResultContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showNext(SearchResult searchResult) {
        if (!isAdded()) {
            return;
        }
        isLoading = false;
        next = 10;
        total = searchResult.getStatuses().size();

        adapter.appendData(searchResult.getStatuses());
    }

    @Override
    public void showEmptyView() {
        contentView.setVisibility(View.GONE);
        errorView.setVisibility(View.GONE);
        loadingView.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showErrorView(String errorMsg) {
        tvError.setText(errorMsg);
        contentView.setVisibility(View.GONE);
        errorView.setVisibility(View.VISIBLE);
        loadingView.setVisibility(View.GONE);
        emptyView.setVisibility(View.GONE);
    }

    @Override
    public void showLoadingView() {
        contentView.setVisibility(View.GONE);
        errorView.setVisibility(View.GONE);
        loadingView.setVisibility(View.VISIBLE);
        emptyView.setVisibility(View.GONE);
    }

    @Override
    public void showContentView() {
        contentView.setVisibility(View.VISIBLE);
        errorView.setVisibility(View.GONE);
        loadingView.setVisibility(View.GONE);
        emptyView.setVisibility(View.GONE);
    }

}

