package com.ifwe.app.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.ifwe.app.R;
import com.ifwe.app.data.model.Status;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yutao .
 */

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.MyVH> {

    private LayoutInflater layoutInflater;
    private Context context;
    private List<Status> statusList;

    public SearchResultAdapter(Context context) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        statusList = new ArrayList<>();
    }

    @Override
    public MyVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = layoutInflater.inflate(R.layout.item_search_result, parent, false);
        return new MyVH(v);
    }

    @Override
    public void onBindViewHolder(final MyVH holder, int position) {
        Status status = statusList.get(position);

        holder.ivPhoto.setImageURI(status.getUser().getProfileImageUrl());
        holder.tvName.setText(status.getUser().getName());
        holder.tvPartnerName.setText(status.getText());
    }

    public void appendData(List<Status> newList) {
        int oldSize = statusList.size();
        statusList.addAll(newList);
        notifyItemRangeInserted(oldSize, newList.size());
    }

    @Override
    public int getItemCount() {
        return statusList.size();
    }

    public List<Status> getStatusList() {
        return statusList;
    }

    static class MyVH extends RecyclerView.ViewHolder {

        @BindView(R.id.twitter_image_view)
        SimpleDraweeView ivPhoto;

        @BindView(R.id.primary_text_view)
        TextView tvName;

        @BindView(R.id.secondary_text_view)
        TextView tvPartnerName;

        public MyVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}

