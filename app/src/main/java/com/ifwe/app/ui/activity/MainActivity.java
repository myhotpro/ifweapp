package com.ifwe.app.ui.activity;

import android.os.Bundle;

import com.ifwe.app.R;
import com.ifwe.app.ui.base.BaseActivity;


public class MainActivity extends BaseActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_search;
    }

}
