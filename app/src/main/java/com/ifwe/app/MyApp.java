package com.ifwe.app;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.ifwe.app.di.component.ApplicationComponent;
import com.ifwe.app.di.component.DaggerApplicationComponent;
import com.ifwe.app.di.module.ApplicationModule;


/**
 * Created by yutao .
 */

public class MyApp extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();

    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
