package com.ifwe.app.data.api;

import com.ifwe.app.data.model.SearchResult;
import com.ifwe.app.data.model.Token;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by yutao
 */

public interface TwitterApi {

    @FormUrlEncoded
    @POST(ApiConstants.TWITTER_TOKEN)
    Observable<Token> getToken(
            @Header("Authorization") String authorization,
            @Field("grant_type") String grantType
    );

    @GET(ApiConstants.TWITTER_SEARCH)
    Observable<SearchResult> search(
            @Header("Authorization") String authorization,
            @Query("q") String keyword
    );
}
