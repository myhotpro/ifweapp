
package com.ifwe.app.data.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Status implements Parcelable{

    private long id;
    private String text;

    @SerializedName("created_at")
    private String createdAt;

    private User user;

    @SerializedName("in_reply_to_screen_name")
    private String inReplyToScreenName;

    public long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public User getUser() {
        return user;
    }

    public String getInReplyToScreenName() {
        return inReplyToScreenName;
    }

    // Parcelable
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeLong(id);
        out.writeString(text);
        out.writeString(createdAt);
        out.writeParcelable(user, flags);
    }

    public static final Parcelable.Creator<Status> CREATOR
            = new Parcelable.Creator<Status>() {
        public Status createFromParcel(Parcel in) {
            return new Status(in);
        }

        public Status[] newArray(int size) {
            return new Status[size];
        }
    };

    private Status(Parcel in) {
        id = in.readLong();
        text = in.readString();
        createdAt = in.readString();
        user = in.readParcelable(User.class.getClassLoader());
    }
}
