package com.ifwe.app.data;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by yutao on 1/8/17.
 */

public class MyPrefs {
    private static final String TOKEN_PREFS_ID = "token_prefs";
    private static final String ACCESS_TOKEN = "access_token";
    private static final String ACCESS_TOKEN_TYPE = "token_status";
    private Context context;

    public MyPrefs(Context context) {
        this.context = context;
    }

    private SharedPreferences getPrefs() {
        return context.getSharedPreferences(TOKEN_PREFS_ID, Context.MODE_PRIVATE);
    }

    public void setAccessToken(String token) {
        getPrefs().edit().putString(ACCESS_TOKEN, token).apply();
    }

    public String getAccessToken() {
        return getPrefs().getString(ACCESS_TOKEN, "");
    }

    public void setTokenType(String tokenType) {
        getPrefs().edit().putString(ACCESS_TOKEN_TYPE, tokenType).apply();
    }

    public String getTokenType() {
        return getPrefs().getString(ACCESS_TOKEN_TYPE, "");
    }
}
