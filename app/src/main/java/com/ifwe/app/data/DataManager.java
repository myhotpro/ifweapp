package com.ifwe.app.data;

import com.ifwe.app.data.api.ApiConstants;
import com.ifwe.app.data.api.TwitterApi;
import com.ifwe.app.data.api.Utils;
import com.ifwe.app.data.model.SearchResult;
import com.ifwe.app.data.model.Token;

import rx.Observable;

/**
 * Created by yutao .
 */

public class DataManager {


    private static final String TAG = DataManager.class.getSimpleName();
    private TwitterApi api;

    public DataManager(TwitterApi api) {
        this.api = api;
    }

    public Observable<Token> getToken() {
        return api.getToken(
                "Basic " + Utils.getBase64String(ApiConstants.BEARER_TOKEN_CREDENTIALS),
                "client_credentials");
    }

    public Observable<SearchResult> search(
            String token,
            String keyword,
            int count) {
        return api.search(token, keyword);
    }

}
