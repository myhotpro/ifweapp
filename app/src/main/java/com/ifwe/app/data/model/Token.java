package com.ifwe.app.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yutao on 1/8/17.
 */

public class Token {

    @SerializedName("token_type")
    public String tokenType;

    @SerializedName("access_token")
    public String accessToken;
}
