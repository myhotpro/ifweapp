package com.ifwe.app.data.api;

import android.util.Base64;

import java.io.UnsupportedEncodingException;

/**
 * Created by yutao on 1/8/17.
 */

public final class Utils {
    public static String getBase64String(String value) {
        String str = "";
        try {
            str = Base64.encodeToString(value.getBytes("UTF-8"), Base64.NO_WRAP);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return str;
    }
}
