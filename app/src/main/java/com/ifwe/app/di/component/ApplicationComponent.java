package com.ifwe.app.di.component;

import android.app.Application;

import com.ifwe.app.data.DataManager;
import com.ifwe.app.data.MyPrefs;
import com.ifwe.app.di.module.ApplicationModule;
import com.ifwe.app.ui.activity.SearchResultActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by yutao .
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    void inject(SearchResultActivity searchResultActivity);
    DataManager dataManager();
    MyPrefs myPrefs();
}