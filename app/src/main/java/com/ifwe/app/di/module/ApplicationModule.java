package com.ifwe.app.di.module;

import android.app.Application;
import android.text.TextUtils;

import com.ifwe.app.data.DataManager;
import com.ifwe.app.data.MyPrefs;
import com.ifwe.app.data.api.ApiConstants;
import com.ifwe.app.data.api.TwitterApi;


import java.io.IOException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by yutao .
 */

@Module
public class ApplicationModule {

    protected final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    Cache provideOkHttpCache(Application application) {
        int cacheSize = 100 * 1024 * 1024; // 100 MB
        Cache cache = new Cache(application.getCacheDir(), cacheSize);
        return cache;
    }

    @Provides
    @Singleton
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }

    @Provides
    @Singleton
    Interceptor provideInterceptor() {
        Interceptor interceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                Response response = chain.proceed(request);

                String cacheControl = request.cacheControl().toString();
                if (TextUtils.isEmpty(cacheControl)) {
                    // cache 5 minute
                    cacheControl = "public, max-age=300";
                }
                return response.newBuilder()
                        .header("Cache-Control", cacheControl)
                        .removeHeader("Pragma")
                        .build();
            }
        };
        return interceptor;
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(
            Cache cache,
            HttpLoggingInterceptor interceptor,
            Interceptor cacheInterceptor) {
        return new OkHttpClient.Builder()
                .addNetworkInterceptor(cacheInterceptor)
                .addInterceptor(interceptor)
                .cache(cache).build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        RxJavaCallAdapterFactory rxJavaCallAdapterFactory =
                RxJavaCallAdapterFactory.create();
        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ApiConstants.TWITTER_BASE_URL)
                .client(okHttpClient)
                .build();
        return retrofit;
    }

    @Provides
    @Singleton
    TwitterApi provideTwitterApi(Retrofit retrofit) {
        return retrofit.create(TwitterApi.class);
    }

    @Provides
    @Singleton
    DataManager provideDataManager(TwitterApi twitterApi) {
        return new DataManager(twitterApi);
    }

    @Provides
    @Singleton
    MyPrefs provideMyPrefs() {
        return new MyPrefs(mApplication);
    }
}
